(in-package #:stride-short-link)

(defun string->char-codes (url)
  (loop :for c :across url
        :collect (char-code c)))

(defun encode-link (url)
  (let ((hash 5381))
    (loop :for c :in (string->char-codes url)
          :do (setq hash (+ (+ (ash hash 5) hash) c)))
    hash))

(defun shorten-link (url)
  (let* ((encoded (encode-link url))
         (gen (random-state:make-generator :mersenne-twister-32 encoded))
         (numbers (loop :for i :from 0 :to 9
                        :collect (random-state:random-int gen 0 36)))
         (*print-base* 36))
    (format nil "~{~a~}" numbers)))
