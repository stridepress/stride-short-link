;;;; stride-short-link.asd

(asdf:defsystem #:stride-short-link
  :description "Describe stride-short-link here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:random-state
               #:hunchentoot
               #:sqlite
               #:yason
               #:drakma
               #:cl-keycloak)
  :components ((:file "package")
               (:file "config")
               (:file "shortened")
               (:file "network")
               (:file "store")))
