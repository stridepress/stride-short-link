FROM phusion/baseimage:latest

RUN apt-get update
RUN apt-get install -y --no-install-recommends git
RUN apt-get install -y --no-install-recommends openssl
RUN apt-get install -y --no-install-recommends curl
RUN apt-get install -y --no-install-recommends libsqlite3-0
RUN apt-get install -y --no-install-recommends sbcl
RUN curl -k -O https://beta.quicklisp.org/quicklisp.lisp
RUN sbcl --load quicklisp.lisp --eval "(quicklisp-quickstart:install)" \
                               --eval "(ql::without-prompting (ql:add-to-init-file))" \
                               --quit

WORKDIR /root/quicklisp/local-projects/
RUN git clone https://github.com/ikbenlike/cl-keycloak.git
WORKDIR /root/quicklisp/local-projects/stride-short-link
COPY . /root/quicklisp/local-projects/stride-short-link
EXPOSE 4242
CMD sbcl --eval "(progn (ql:quickload \"stride-short-link\") (loop))"
