(in-package #:stride-short-link)

(defvar *database* (sqlite:connect "link-store.db" :busy-timeout 1000000))

(defun resolve-code (code)
  (sqlite:execute-single
    *database*
    "select link from links where code = ?" code))

(defun store-code (code link user)
  (sqlite:execute-non-query
    *database*
    "insert into links (code, link, user) values (?, ?, ?)" code link user))
