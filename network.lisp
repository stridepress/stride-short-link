(in-package #:stride-short-link)

(defvar *html-shorten-page*
  "<!DOCTYPE html>
   <html>
   <head>
       <meta charset=\"UTF-8\"/>
       <link rel=\"stylesheet\" href=\"styles.css\"/>
       <link href='https://fonts.googleapis.com/css?family=Bitter&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
       <title>Link Shortener</title>
   </head>
   <body>
       <nav id=\"horizontal_nav\">
           <p id=\"nav_links\">
               <a href=\"https://stride.press\"><button type=\"button\" id=\"button\">Home</button></a>
               <a href=\"https://s.stride.press/manage\"><button type=\"button\" id=\"button\">Manage</button></a>
           </p>
       </nav><p id=\"shortened_link\">Your shortened link is <a href=\"//~a\">~a</a></body></html>")

(cl-keycloak:with-client client (:client-id "food" :server-url "https://cloak.stride.press/auth"
                                 :client-secret "e6a38d58-f01e-4e80-9c9c-a6a7bc5a21d3"
                                 :key-realm "master")
  (let* ((server-redirect +oauth-redirect+)
         (home-redirect +home-redirect+)
         (auth-redirect (cl-keycloak:get-auth-redirect client :redirect server-redirect)))

    (hunchentoot:define-easy-handler (login :uri "/login") ()
      (hunchentoot:redirect auth-redirect :protocol :https))

    (hunchentoot:define-easy-handler (shortener :uri "/shorten") (link)
      (when (not (multiple-value-bind (a b)
                     (hunchentoot:session-value :token)
                   (and a b)))
        (hunchentoot:redirect auth-redirect :protocol :https))
      (setf (hunchentoot:content-type*) "text/html")
      (let* ((code (shorten-link link))
             (url (concatenate 'string "s.stride.press/" code))
             (user-sub (gethash "sub" (cl-keycloak:get-user-info client (hunchentoot:session-value :token)))))
        (store-code code link user-sub)
        (format nil *html-shorten-page* url url)))

    (hunchentoot:define-easy-handler (auth-endpoint :uri "/oauth") (code)
      (let ((response (cl-keycloak:request-token client code server-redirect)))
        (setf (hunchentoot:session-value :token) (gethash "access_token" response))
        (setf (hunchentoot:session-value :refresh) (gethash "refresh_token" response))
        (hunchentoot:redirect home-redirect)))

    (hunchentoot:define-easy-handler (logout :uri "/logout") ()
      (let ((response (cl-keycloak:logout-user client (hunchentoot:session-value :refresh))))
        (setf (hunchentoot:session-value :token) nil)
        (setf (hunchentoot:session-value :refresh) nil)
        (if response
            (with-output-to-string (out) (yason:encode response out))
            (hunchentoot:redirect home-redirect))))

    (hunchentoot:define-easy-handler (manage :uri "/manage") ()
      (format t "Token manage: ~a~%~%~%" (hunchentoot:session-value :token))
      (cl-keycloak:get-user-info client (hunchentoot:session-value :token)))))

(defun resolve-shortened-link ()
  (let* ((redirect-code (subseq (hunchentoot:request-uri* hunchentoot:*request*) 1))
         (resolve-link (resolve-code redirect-code)))
    (format t "redirect-link: ~a" resolve-link)
    (if (string= (subseq resolve-link 0 4) "http")
        (hunchentoot:redirect resolve-link)
        (hunchentoot:redirect "" :protocol :http :host resolve-link))))

(push (hunchentoot:create-regex-dispatcher "^/([a-zA-Z]|[0-9]){10}$" 'resolve-shortened-link)
      hunchentoot:*dispatch-table*)

(push (hunchentoot:create-static-file-dispatcher-and-handler "/" "index.html" "text/html")
      hunchentoot:*dispatch-table*)

(push (hunchentoot:create-static-file-dispatcher-and-handler "/styles.css" "styles.css")
      hunchentoot:*dispatch-table*)

(defvar *acceptor* (make-instance 'hunchentoot:easy-acceptor :port +port+))

(hunchentoot:start *acceptor*)
