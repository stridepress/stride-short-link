(in-package #:stride-short-link)

(defvar +oauth-redirect+ "http://localhost:4242/oauth")
(defvar +home-redirect+ "http://localhost:4242/")
(defvar +port+ 4242)
